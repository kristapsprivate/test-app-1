/**
 * Geta test 2 js
 * @description This application is not fully supported by all actual browser versions.
 */

let _app;

class app {

    constructor() {
        _app = this;
        this.apiKey = site.youtubeApiKey;
        this.playlistId = site.youtubePlaylistId;
        this.$videoMenu = document.querySelector('.js-video-menu');
        this.$videoPlayer = document.getElementById('player');
        this.videoMenuItemTpl = document.querySelector('.js-video-item-tpl').innerHTML;
        this.initVideoGallery();
    }

    initVideoGallery() {
        let url = 'https://www.googleapis.com/youtube/v3/playlistItems?'
            + 'part=snippet'
            + '&playlistId=' + this.playlistId
            + '&key=' + this.apiKey
            + '&maxResults=' + 10;

        fetch(url)
            .then(function (response) {
                return response.json();
            })
            .then(function (data) {
                //load first page only, no need for pagination with simple example
                _app.loadPlaylist(data.items);
            })
            .catch(function () {
                //no error handling for this case
            });

        this.$videoMenu.addEventListener('click', function (e) {
            setTimeout(() => window.scroll(0, 0), 100);
        });
    }

    loadPlaylist(items) {
        items.forEach(function (item, key) {
            let itemUrl = 'https://www.youtube.com/embed/' + item.snippet.resourceId.videoId
                + '?list=' + _app.playlistId + '&autoplay=1&controls=1';

            if (key === 0) {
                _app.$videoPlayer.src = itemUrl;
            }
            _app.$videoMenu.innerHTML += _app.videoMenuItemTpl
                .replace('[url]', itemUrl)
                .replace('[title]', item.snippet.title)
                .replace('[thumbUrl]', item.snippet.thumbnails.default.url);
        });
    }
}

new app();
