﻿Task 1

One of the best practices for building mobile design would be to do mobile first, then grow up to tablet and desktop screens. Usually how things floats and adapts to different screens are already defined by designers. There are times when you are better to use grid, for example sass mixins but in most cases it is not necessary. After finishing project is recommended to validate html markup and run some speed test for optimization, maybe minimize assets (js, css, images). Fonts also can be base64 encoded for optimization.

In provided example I see that breadcrumbs are above the main menu, better would be to put it before „Welcome to Our Wiki!” heading title.
Print button is located after „Log out” link, it should be at the same place where is the mail icon.
Link „Add comment” also can be placed at the same section where „Add Child Page” is located.
It seems like content administration is built in frontend interface that actually make things even more difficult for more advanced solutions.
In today’s time when ajax is often used I think there is not necessary „Search” button (if it’s not a designer’s opinion), suggestions can be helpful for a search input.

For mobile devices a hamburger icon could be an option for menu and search together. Header might be fixed to top when user scrolls down. I would put „Wiki” logo in left or right side in the header.
So if we are taken out left sided menu and logo we have more space for the content. There will not be much changes, it seems to have ability to shrink easily, only comments menu zone might be in vertical and not horizontal order. Breadcrumbs might be hidden if the menu structure would be integrated in mobile menu. The best solution is when the same html menu elements are styled for all devices but it is not always an option. For this case it might be problematic, so there would be 2 different menus – one for mobile and one for desktop.

For tablets there are not so many changes only if it’s not in a designe. Content will easily expand to window size, no need for grid. If breadcrumbs was hidden for mobile, we can show it for tablet because we have more space. Maybe even header menu might be visible without hamburger icon including search bar or icon that expands on click.
